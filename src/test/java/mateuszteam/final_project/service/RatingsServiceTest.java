package mateuszteam.final_project.service;

import mateuszteam.final_project.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class RatingsServiceTest {

    @Autowired
    RatingsService ratingsService;

    @Test
    void should_throw_exception(){

        assertThrows(ResourceNotFoundException.class, () -> ratingsService.getAllRatingsForSingleMovie(5L));
    }




}
